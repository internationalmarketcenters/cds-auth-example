'use strict';

if (process.argv.length !== 3) {
  console.error(`usage: ${process.argv[1]} <path-to-private-key>`);
  process.exit(1);
}

// https://github.com/auth0/node-jsonwebtoken
// Use for creating JWTs.
const jsonwebtoken = require('jsonwebtoken');

// Set up the JWT claims (see https://jwt.io).
const now = new Date().getTime();
const exp = now + 3600000; // One hour, milliseconds.

const claims = {
  // Issuer: This application's name.
  iss : 'cdsreg.com',
  
  // Scope: Requesting permission to the v1.0/Search/AlumniSearch API.
  scope : 'v1.0/Search/AlumniSearch',

  // Audience: This token's intended for the v1.0/OAuth2/Token API.
  aud : 'v1.0/OAuth2/Token',

  // Issued at: Now.
  iat : now,

  // Expires: One hour from now.
  exp : exp
};

// Load the private key.
const keyPath = process.argv[2];
const fs      = require('fs');
const privKey = fs.readFileSync(keyPath);

// Create and sign the JWT.
const jwt = jsonwebtoken.sign(claims, privKey, {algorithm: 'RS256'});

console.log('Created local JWT.');
console.log(jwt);

// Request an auth token from the OAuth2 endpoint.
const request = require('request');
const options = {
  uri    : 'https://cdsapi.americasmart.com/v1.0/Oauth2/Token',
  method : 'POST',
  json   : {
    grant_type : 'urn:ietf:params:oauth:grant-type:jwt-bearer',
    assertion  : jwt
  }
};

request(options, function(err, resp, body) {
  if (resp.statusCode < 200 || resp.statusCode >= 300) {
    console.error('Error response from token endpoint.');
    console.error(body);
  }
  else {
    console.log('Received the following response from the OAuth2/Token endpoint.');
    console.log(body);
  }
});

