'use strict';

const util = require('util');

if (process.argv.length !== 4) {
  console.error(`usage: ${process.argv[1]} <access-token> <email>`);
  process.exit(1);
}

const accessToken = process.argv[2];
const email       = process.argv[3];

// Hit the AlumniSearch API, suppling the user's search criteria
// and authorization token.
const request = require('request');
const options = {
  uri: 'https://cdsapi.americasmart.com/v1.0/Search/AlumniSearch',
  headers: {
    Authorization: `Bearer ${accessToken}`,
    'Content-Type': 'application/json'
  },
  method: 'GET',
  qs: {
    email
  }
};

request(options, function(err, resp, body) {
  if (resp.statusCode < 200 || resp.statusCode >= 300) {
    console.error('Error response from token endpoint.');
    console.error(body);
  }
  else {
    const companies = JSON.parse(body);

    console.log('Received the following response from the Search/AlumniSearch endpoint.');
    console.log(util.inspect(companies, {depth: null, compact: false}));
  }
});

